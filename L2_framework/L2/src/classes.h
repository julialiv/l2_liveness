#pragma once

#include <string>
#include <map>
#include <set>
#include <vector>
#include <classes.h>

using namespace std;

namespace L2 {

	static std::map<string, string> reg64to8 = {{"r10","r10b"}, {"r13","r13b"},{"r12","r12b"}, {"rdi","dil"}, {"r11","r11b"}, {"r14","r14b"}, {"r15","r15b"}, {"r8","r8b"}, {"r9","r9b"}, {"rax","al"}, {"rbp","bpl"}, {"rbx","bl"}, {"rcx","cl"}, {"rsi","sil"}, {"rdx","dl"}};

	enum LineType
	{
		Assign,
		Arith,
		decremIncrem,
		Return,
		lineLabel,
		Comparison,
		conditional_jump,
		call_line,
		leaType
		// ADD MORE HERE
	};

	enum Type
	{
		mem,
		reg,
		value,
		label,
		operation,
		g2,
		call_type,
		var,
		stackArg
	};

	enum cmpType
	{
		l,
		le,
		e
	};

	class Item
	{
		// str will be either a: register or label
		// val will be either a: mem offset or value
		public:
			std::string str;
			int64_t val;
			Type t;
	};

	class Instruction {
		public:
			LineType type;
			std::set<int> successor_set;
	};

	class Ln_Label: public Instruction {
		public:
			string lab;
			bool goToFlag;
	};

	class LEA: public Instruction {
		public:
			Item dst;
			Item src1;
			Item src2;
			int64_t offset;
	};

	class Assignment: public Instruction
	{
		public:
			Item dst;
			Item src;
	};

	class Compare: public Instruction
	{
		public:
			Item src1; // Middle value (always a register)
			Item src2; // Rightmost value
			Item dst;
			cmpType c;
	};

	class cJump: public Instruction
	{
		public:
			Item src1;
			Item src2;
			Item label1;
			Item label2;
			cmpType c;
	};

	class Arithmetic: public Instruction
	{
		public:
			Item dst;
			Item src;
			char op;
	};

	class Call_inst: public Instruction
	{
		public:
			Item fn;
			int num_args;
	};

	class DI: public Instruction
	{
		public:
			Item dst;
			char op;
	};

	struct inOut
	{
		std::set<std::string> in;
		std::set<std::string> out;
	};

	class DataFlowResult
	{
		public:
			std::vector<inOut*> inOutPerLine;
	};

}
