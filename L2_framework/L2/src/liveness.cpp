#include <liveness.h>
#include <iostream>
#include <typeinfo>
#include <iterator>
#include <vector>
using namespace std;

namespace L2 {

  struct genKill {
    set<string> gen;
    set<string> kill;
  };

  vector<genKill*> globalGenKill;

  string args [6] = { "rdi", "rsi", "rdx", "rcx", "r8", "r9" };
  string caller_save [9] = { "r10", "r11", "r8", "r9", "rcx", "rdi", "rdx", "rsi", "rax"};
  string callee_save [7] = { "r12", "r13", "r14", "r15", "rbp", "rbx", "rax" };

  // assuming we only need to store registers and variables
  void setGen(Item src, genKill *GK){
    if (src.t == reg || src.t == var || src.t == mem) {
      if (src.str != "rsp")
        GK->gen.insert(src.str);
    }
    //cout << "set gen" << endl;
  }

  void setKill(Item dst, genKill *GK){
    if (dst.t == reg || dst.t == var)
      GK->kill.insert(dst.str);
  }

  void computeGenKill (Program p, Function *f) {
    ////cout << "num instructions: " << (f->lines).size() << endl;
    for (int i = 0; i < (f->lines).size(); i++) {
        ////cout << "computing genKill for instruction #" << i << endl;
        Instruction* instruct = (f->lines).at(i);
        genKill* GK = new genKill;
        L2::LineType t = instruct->type;
        if (t==Assign){
            Assignment* as = static_cast<Assignment*>(instruct);
            setGen(as->src, GK);
            setKill(as->dst, GK);
        } else if (t == Arith){
            Arithmetic* ar = static_cast<Arithmetic*>(instruct);
            setGen(ar->src, GK);
            setGen(ar->dst, GK);
            setKill(ar->dst, GK);
        } else if (t==decremIncrem){
            DI* di = static_cast<DI*>(instruct);
            setGen(di->dst, GK);
            setKill(di->dst, GK);
        } else if (t==Comparison){
            Compare* c = static_cast<Compare*>(instruct);
            setGen(c->src1, GK);
            setGen(c->src2, GK);
            setKill(c->dst, GK);
        } else if (t==conditional_jump){
            cJump* cj = static_cast<cJump*>(instruct);
            setGen(cj->src1, GK);
            setGen(cj->src2, GK);
        } else if (t==call_line){
            Call_inst* cl = static_cast<Call_inst*>(instruct);
            setGen(cl->fn, GK);
            for (int j = 0; j < cl->num_args; j++){
              GK->gen.insert(args[j]);
            }
            //GK->kill.insert("rax");
            for (string reg: caller_save)
              GK->kill.insert(reg);
        } else if (t==leaType){
            LEA* lea = static_cast<LEA*>(instruct);
            setGen(lea->src1, GK);
            setGen(lea->src2, GK);
            setKill(lea->dst, GK);
        }
        else if (t==Return){
            //GK->gen.insert("rax");
            for (string reg: callee_save)
              GK->gen.insert(reg);
        }
        globalGenKill.push_back(GK);
    }
  }

  set<string> getDif(set<string> set1, set<string> set2) {
	set<string> dif;
	for (auto el: set1) {
		bool found = false;
		for (auto el2 : set2) {
			if (el == el2)
				found = true;
		}
		if (!found)
			dif.insert(el);
		//if (set2.find(el) != set2.end())
		//	dif.insert(el);
	}
	return dif;
  }

  DataFlowResult* computeLivenessAnalysis(Program p, Function *f) {
    // Step 1: computer gen, kill for each instruction
    //////cout << "about to compute gen and kill" << endl;
    computeGenKill(p, f);
    // //cout << "Gen/Kill contain:" << endl;
    // for (auto gk : globalGenKill) {
    //   set<string> myGen = gk->gen;
    //   std:://cout << "generating:";
    //   for(string str : myGen) {
    //     //cout<<' ' << str;
    //   }
    //   //cout << "\nkilling:";
    //   set<string> myKill = gk->kill;
    //   for(string str : myKill) {
    //     //cout<<' ' << str;
    //   }
    //   //cout << endl;
    // }
    ////cout << "computed gen and kill" << endl;

    // Step 2: Create empty in and out sets for each instruction
    DataFlowResult* df= new DataFlowResult;
    ////cout << "making blank sets  " << endl;
    for (int i = 0; i < (f->lines).size(); i=i+1) {
        inOut* inOutSets = new inOut;
        inOutSets->in = {};
        inOutSets->out = {};
        df->inOutPerLine.push_back(inOutSets);
    }
    ////cout << "made blank sets for in and out " << endl;

    // Step 3: do while loop
    bool hasChanged = false;
    int count = 0;
    do {
      hasChanged = false;
      for (int i = 0; i < (f->lines).size(); i=i+1) {
        //cout << "filling in out for instruction #" << i << "\n" << endl;
        Instruction* instruct = (f->lines).at(i);
        inOut * inOut_i =  df->inOutPerLine[i];
        set<string> prevIn = inOut_i->in;
        set<string> prevOut = inOut_i->out;

        ////cout << "created prevs to compare" << endl;
        // add out[i] - kill[i] to in[i]
        set <string> killSet = globalGenKill[i]->kill;
        set<string> dif = getDif(inOut_i->out, killSet);
        inOut_i->in.insert(dif.begin(), dif.end());
        ////cout << "add out[i] - kill[i] to in[i]" << endl;
        // add gen[i] to in[i]
        inOut_i->in.insert(globalGenKill[i]->gen.begin(), globalGenKill[i]->gen.end());
        ////cout << "size of in is now: " << inOut_i->in.size() << endl;

        // add in[successors] to out[i]
        for (auto s: instruct->successor_set) {
          ////cout << "going through successors" << endl;
          inOut* inOut_s = df->inOutPerLine[s];

          ////cout<<"Size of in set of line #" << s << " is " << inOut_s->in.size()<<endl;
          ////cout<<"Size of out set of line #" << s << " is " << inOut_s->out.size()<<endl;

          inOut_i->out.insert(inOut_s->in.begin(), inOut_s->in.end());
        }
        // cout << "added in successor shit" << endl;
        // cout << "Previous in: ";
        //for(string str : prevIn) {
        //   cout<<' ' << str;
        // }
        // cout << "\nNew in: ";
         //for(string str : df->inOutPerLine[i]->in) {
         //  cout<<' ' << str;
         //}
       // cout << "\nPrevious out: ";
        //for(string str : prevOut) {
        //  cout<<' ' << str;
       // }
        //cout << "\nNew out: ";
        //for(string str : df->inOutPerLine[i]->out) {
        //  cout<<' ' << str;
        //}
        if (prevIn != df->inOutPerLine[i]->in || prevOut != df->inOutPerLine[i]->out)
          hasChanged = true;
        //cout << "\nhaschanged is: " << hasChanged << endl;
      }
      //count++;
    } while (hasChanged);
    ////cout << "returned df!" << endl;
    return df;
  }

}
