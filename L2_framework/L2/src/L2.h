#pragma once

#include <vector>
#include <classes.h>

namespace L2 {

  struct L2_item {
    std::string labelName;
  };

  struct Function{
    std::string name;
    int64_t arguments;
    int64_t locals;
    std::vector<Instruction *> lines;
  };


  struct Program{
    std::string entryPointLabel;
    std::vector<L2::Function *> functions;
  };

} // L5
