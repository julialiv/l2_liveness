#include <string>
#include <vector>
#include <utility>
#include <algorithm>
#include <set>
#include <iterator>
#include <cstring>
#include <cctype>
#include <cstdlib>
#include <stdint.h>
#include <unistd.h>
#include <iostream>

#include <parser.h>
#include <code_generator.h>
#include <liveness.h>
#include <successors.h>

using namespace std;

void print_help (char *progName){
  std::cerr << "Usage: " << progName << " [-v] [-g 0|1] [-O 0|1|2] [-s] [-l 1|2] SOURCE" << std::endl;
  return ;
}

const string toAString(L2::DataFlowResult *liveness)
{
  vector<L2::inOut*> inOutPerLine = liveness->inOutPerLine;
  string ssIn;
  string ssOut;
  ssIn = "(in \n";
  ssOut = "(out \n";
  for (auto inOut: inOutPerLine){
    ssIn += "(";
    ssOut += "(";
    for (auto strI: inOut->in) {
      ssIn += strI + " "; // extra space at end -- does it matter??
    }
    for (auto strO: inOut->out) {
      ssOut += strO + " ";
    }
    ssIn += ")\n";
    ssOut += ")\n";
  }
  ssIn += ") \n";
  ssOut += ") \n";
  return "(\n" + ssIn + "\n" + ssOut + "\n)";
}

int main(int argc, char **argv){
  bool enable_code_generator = false;
  bool spill_only = false;
  int32_t liveness_only = 1;
  int32_t optLevel = 0;
  //cout << "in main" << endl;
  /* Check the input.
   */
  //Utils::verbose = false;
  if( argc < 2 ) {
    print_help(argv[0]);
    return 1;
  }
  int32_t opt;
  while ((opt = getopt(argc, argv, "vg:O:sl:")) != -1) {
    switch (opt){
      case 'l':
        //cout << "in liveness_only" << endl;
        liveness_only = strtoul(optarg, NULL, 0);
        break ;

      case 's':
        spill_only = true;
        break ;

      case 'O':
        optLevel = strtoul(optarg, NULL, 0);
        break ;

      case 'g':
        enable_code_generator = (strtoul(optarg, NULL, 0) == 0) ? false : true ;
        break ;

      case 'v':
      //TODO
        break ;

      default:
        print_help(argv[0]);
        return 1;
    }
  }

  /*
   * Parse the input file.
   */
  L2::Program p;
  if (spill_only){

    /*
     * Parse an L2 function and the spill arguments.
     */
    //p = L2::parse_spill_file(argv[optind]);

  } else if (liveness_only){

    /*
     * Parse an L2 function.
     */
    //cout << "about to parse_function_file" << endl;
    p = L2::parse_function_file(argv[optind]);

  } else {

    /*
     * Parse the L2 program.
     */
    p = L2::parse_file(argv[optind]);
  }

  /*
   * Special cases.
   */
  if (spill_only){

    /*
     * Spill.
     */
    //L2::REG_spill(p, p.functions[0], p.spill.varToSpill, p.spill.prefixSpilledVars);
  }

  if (liveness_only){
    //cout << "starting to compute successors" << endl;

    computeSuccessors (p);

    //cout << "just computed successors" << endl;

    for (auto f : p.functions){

      /*
       * Compute the liveness analysis.
       */
      //cout << "about to compute liveness" << endl;
      auto liveness = computeLivenessAnalysis(p, f);
      //cout << "computed LIVENESS!" << endl;
      /*
       * Print the liveness.
       */
      //cout << liveness->toString(f, liveness_only > 1) << endl;
      //cout << "printing in and out" << endl;
      cout << toAString(liveness) << endl;

      /*
       * Free the memory.
       */
      delete liveness;
    }

    return 0;
  }

  /*
   * Generate the code.
   */
  if (enable_code_generator){
    //L2::L2_generate_code(p);
  }

  return 0;
}
