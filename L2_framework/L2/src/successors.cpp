//each instruction holds a set of possible successors

#include <successors.h>
#include <map>
#include <iostream>

using namespace std;

// Store index of the successor(s) line
// Switch on instruction type
// Successors

namespace L2 {
  // Connect Ln_Labels to their corresponding Instruction indices for use in computeSuccessors
  std::map<string,int> label2index (Program p) {
	  std::map<string,int> indexLabels;
	  for (auto f: p.functions){
	    for (int i = 0; i < f->lines.size(); i = i+1) {
	    	// Only process line labels
			Instruction * line = f->lines[i];
	    	if(f->lines[i]->type == lineLabel){
	    		Ln_Label* line_label = static_cast<Ln_Label*>(line);
	    		indexLabels[line_label->lab] = i;
	    	}
	    }
  	   }
    return indexLabels;
   }

  void computeSuccessors (Program p) {
      //cout << "entered compute successors" << endl;
  	  std::map<string,int> labelIndices = label2index(p);

      //cout << "about to enter for loop" << endl;
      for (auto f: p.functions){
        //cout << "in for loop in successors" << endl;
        int count = 0;
      	// For each instruction, compute and save successors
        for (auto instruct : f->lines) {
           //cout << "instruction #" << count << endl;
        	 switch(instruct->type){
                //cout << "instruction type" << instruct->type << endl;
                case conditional_jump: {
                  //cout << "in conditional_jump" << endl;
                  cJump* cj = static_cast<cJump*>(instruct);
                  int l1 = labelIndices[cj->label1.str];
                  int l2 = labelIndices[cj->label2.str];
                  cj->successor_set.insert(l1);
                  cj->successor_set.insert(l2);
                  break;
                }
                // case call_line: {
                //   //cout << "in call line" << endl;
                //   Call_inst* cl = static_cast<Call_inst*>(instruct);
                //   if (cl->fn.t == label){
                //     int l1 = labelIndices[cl->fn.str];
                //     cl->successor_set.insert(l1);
                //   }
                //   else {
                //     instruct->successor_set.insert(count+1);
                //   }
                //   // NOTE: WE NEED TO HANDLE INDIRECT JUMPS (TO REGISTER)
                //   break;
                // }
                case lineLabel: {
                  //cout << "in line label" << endl;
                  Ln_Label* l = static_cast<Ln_Label*>(instruct);
                  if (l->goToFlag){
                      int l1 = labelIndices[l->lab];
                      l->successor_set.insert(l1);
                  }
                  else {
                      instruct->successor_set.insert(count+1);
                  }
                  break;
                }
                case Return: {
                  //cout << "return" << endl;
                  instruct->successor_set = {};
                  break;
                }
                default:
                  //cout << "just adding next line" << endl;
                  instruct->successor_set.insert(count+1);
                  break;
              }
            count++;
        }
      }
  }
}
