#pragma once

#include <L2.h>
#include <map>

namespace L2{

  void computeSuccessors (Program p);
  std::map<string,int> label2index (Program p);

}
