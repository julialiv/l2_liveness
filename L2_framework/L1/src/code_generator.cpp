#include <string>
#include <iostream>
#include <fstream>

#include <code_generator.h>
#include <classes.cpp>

using namespace std;

namespace L1{

  string generate_item_syntax (Item i) {
	switch(i.t) {
		case mem:
			return to_string(i.val) + "(%" + i.str + ")";
		case reg:
			return "%" + i.str;
		case value:
			return "$" + to_string(i.val);
		case label:
			i.str = "$_" + i.str.substr(1);
			return i.str;
	}
	return "error: no type for item"; 
  }

 string generate_op_syntax(char c){
	switch(c){
		case '+':
			return "addq";
		case '-':
			return "subq";
		case '*':
			return "imulq";
		case '&':
			return "andq";
		case '<':
			return "salq";
		case '>':
			return "sarq";
	}
	return "error: no type for op syntax";
}

string generate_cjump(cmpType c, bool flipped){
	if (!flipped){
		switch(c){
			case l:
				return "jl";
			case le:
				return "jle";
			case e:
				return "je";
		}	
	}
	else {
		switch(c){	
			case l:
				return "jg";
			case le:
				return "jge";
			case e:
				return "je";
		}

	}
	return "error in generate cmp";
}
string generate_cmp(cmpType c, bool flipped){
	if (!flipped){
		switch(c){
			case l:
				return "setl";
			case le:
				return "setle";
			case e:
				return "sete";
		}	
	}
	else {
		switch(c){	
			case l:
				return "setg";
			case le:
				return "setge";
			case e:
				return "sete";
		}

	}
	return "error in generate cmp";
}

string compute_comparison(int64_t val1, int64_t val2, cmpType c){
	switch(c){
		case l:
			return to_string(val1 < val2);
		case le:
			return to_string(val1 <= val2);
		case e:
			return to_string(val1 == val2);
	}
	return "error in computing comparison";
}

string generate_di_syntax(char c){
	switch(c){
		case '+':
			return "inc";
		case '-':
			return "dec";
	}
	return "error: dec inc error";
}

int get_stack_num(int loc, int arg){
	if (arg > 6){
		return (loc + (arg-6))*8;
	} else{
		return loc*8;
	}
}

string generate_sop_syntax(Item i){
	return "%" + reg64to8[i.str];	
}

  void generate_code(Program p){

    /* 
     * Open the output file.
     */ 
    std::ofstream outputFile;
    outputFile.open("prog.S");
   
    /* 
     * Generate target code
     */ 
    // Hardcode the prog.S file
    outputFile << ".text \n .globl go \n";
    outputFile << "go: \n";
    outputFile << "pushq %rbx \n";
    outputFile << "pushq %rbp \n";
    outputFile << "pushq %r12 \n";
    outputFile << "pushq %r13 \n";
    outputFile << "pushq %r14 \n";
    outputFile << "pushq %r15 \n";
    outputFile << "call _" + p.entryPointLabel.substr(1) + "\n";
    outputFile << "popq %r15 \n";
    outputFile << "popq %r14 \n";
    outputFile << "popq %r13 \n";
    outputFile << "popq %r12 \n";
    outputFile << "popq %rbp \n";
    outputFile << "popq %rbx \n";
    outputFile << "retq \n";
    
    
    // Loop through each line and print out the translation
    for(int i=0; i < p.functions.size(); i++){
         outputFile << "_" + p.functions[i]->name.substr(1) + ":\n";
	int mov_stack = get_stack_num(p.functions[i]->locals, p.functions[i]->arguments);
	outputFile << "subq $" << 8*p.functions[i]->locals << ", %rsp #allocate spill \n"; 
 	   for (int j=0; j < p.functions[i]->lines.size(); j++) {
		Instruction* line = p.functions[i]->lines[j];
		switch(line->type) {
			case LineType::Assign:
				outputFile << "movq " << generate_item_syntax(static_cast<Assignment*>(line)->src) << ", " << generate_item_syntax(static_cast<Assignment*>(line)->dst);
				outputFile << " \n";
				break;
			case LineType::Return:
   				outputFile << "addq $" << mov_stack << ", %rsp ";
				outputFile << "#Free stack locations \n"; 
				outputFile << "retq \n";
				break;
			case LineType::Arith:{
				Arithmetic* a = static_cast<Arithmetic*>(line);
				if (a->src.t == reg && (a->op == '>' || a->op == '<'))
					outputFile << generate_op_syntax(a->op) << " " << generate_sop_syntax(a->src) << ", " << generate_item_syntax(a->dst) << "\n";
				else if (a->op == '<' || a->op == '<')
					outputFile << generate_op_syntax(a->op) << " " << generate_item_syntax(a->src) << ", " << generate_item_syntax(a->dst) << "\n";
				else
					outputFile << generate_op_syntax(a->op) << " " << generate_item_syntax(a->src) << ", " << generate_item_syntax(a->dst) << "\n";
				break;
			}
			case LineType::decremIncrem:
				outputFile << generate_di_syntax(static_cast<DI*>(line)->op) << " " << generate_item_syntax(static_cast<DI*>(line)->dst) << "\n";
				break; 
			case LineType::lineLabel:{
				Ln_Label* l = static_cast<Ln_Label*>(line);
				if (l->goToFlag)
					outputFile << "jmp _" << l->lab.substr(1) << " \n";
				else
					outputFile << "_" << l->lab.substr(1) << ": \n";
				break;
			}
			case LineType::Comparison:{
				Compare* cm = static_cast<Compare*>(line);
				bool flipped = false;
				if (cm->src1.t == value && cm->src2.t == value){
					string ans = compute_comparison(cm->src1.val, cm->src2.val, cm->c);
					outputFile << "movq $" << ans << ", " << generate_item_syntax(cm->dst) << "\n";
			}
				else{
					if (cm->src1.t != reg){
						Item temp = cm->src1;
						cm->src1 = cm->src2;
						cm->src2 = temp;
						flipped = true;

					}
					outputFile << "cmpq " << generate_item_syntax(cm->src2) << ", " << generate_item_syntax(cm->src1) + "\n";
					outputFile << generate_cmp(cm->c, flipped) << " " << generate_sop_syntax(cm->dst) << "\n";
					outputFile << "movzbq " << generate_sop_syntax(cm->dst) << ", " << generate_item_syntax(cm->dst) << "\n";}
		
				break;
			}
			case LineType::conditional_jump:{
				cJump* cj = static_cast<cJump*>(line);
				bool flipped = false;
				if (cj->src1.t == value && cj->src2.t == value){
					string ans = compute_comparison(cj->src1.val, cj->src2.val, cj->c);
					if (ans[0] == '0')
						outputFile << "jmp _" << cj->label2.str.substr(1) << "\n";
					else
								outputFile << "jmp _" << cj->label1.str.substr(1) << "\n";
				}
				else{
					if (cj->src1.t != reg){
						Item temp = cj->src1;
						cj->src1 = cj->src2;
						cj->src2 = temp;
						flipped = true;

					}
					outputFile << "cmpq " << generate_item_syntax(cj->src2) << ", " << generate_item_syntax(cj->src1) + "\n";
					outputFile << generate_cjump(cj->c, flipped) << " _" << cj->label1.str.substr(1) << "\n";
					outputFile << "jmp _" << cj->label2.str.substr(1) << "\n";
				}
				break;
			}
			case LineType::call_line:{
				Call_inst* ci = static_cast<Call_inst*>(line);
				if (ci->fn.t == call_type){
					if (ci->fn.str[5] == '-')
						ci->fn.str = "array_error";
						
					outputFile << "call " << ci->fn.str << "\n";
				} else if (ci->fn.t == label){
					outputFile << "subq $" << get_stack_num(0, ci->num_args)+8 << ", %rsp \n";
					outputFile << "jmp _" << ci->fn.str.substr(1) << "\n";
				} else {
					outputFile << "subq $" << get_stack_num(0, ci->num_args)+8 << ", %rsp \n";

					outputFile << "jmp *" << generate_item_syntax(ci->fn) << "\n";
				}

				break;
			}
			case LineType::leaType:{
				LEA* l = static_cast<LEA*>(line);
				outputFile << "lea (" << generate_item_syntax(l->src1) << ", " << generate_item_syntax(l->src2) << ", " << l->offset << "), " << generate_item_syntax(l->dst) << "\n";
			}
		}
		free(line);
	 }
	free(p.functions[i]);
    } 
     
    /* 
     * Close the output file.
     */ 
    outputFile.close();
   
    return ;
  }
}
