#include <string>
#include <vector>
#include <utility>
#include <algorithm>
#include <set>
#include <iterator>
#include <cstring>
#include <cctype>
#include <cstdlib>
#include <stdint.h>
#include <assert.h>

#include <L1.h>
#include <parser.h>

#include <classes.cpp>

#include <tao/pegtl.hpp>
#include <tao/pegtl/analyze.hpp>
#include <tao/pegtl/contrib/raw_string.hpp>

namespace pegtl = tao::TAO_PEGTL_NAMESPACE;

using namespace pegtl;
using namespace std;

namespace L1 {

  /* 
   * Data required to parse
   */ 
  std::vector< Item> parsed_registers;

  /* 
   * Grammar rules from now on.
   */


  struct comment: 
    pegtl::disable< 
      TAOCPP_PEGTL_STRING( "//" ), 
      pegtl::until< pegtl::eolf > 
    > {};

  struct seps: 
    pegtl::star< 
      pegtl::sor< 
        pegtl::ascii::space, 
        comment 
      > 
    > {};

  struct label_rule:
    pegtl::seq<
      pegtl::one<':'>,
      pegtl::plus< 
        pegtl::sor<
          pegtl::alpha,
          pegtl::one< '_' >
        >
      >,
      pegtl::star<
        pegtl::sor<
          pegtl::alpha,
          pegtl::one< '_' >,
          pegtl::digit
        >
      >
    > {};

 // aop includes sop
 struct aop:
	pegtl::sor<
		pegtl::one<'+'>,
		pegtl::one<'-'>,
		pegtl::one<'*'>,
		pegtl::one<'&'>,
		TAOCPP_PEGTL_STRING( ">>" ),
		TAOCPP_PEGTL_STRING( "<<" )
	> {};

struct cmp:
	pegtl::sor<
		TAOCPP_PEGTL_STRING( "<=" ),
		TAOCPP_PEGTL_STRING( "<" ),
		TAOCPP_PEGTL_STRING( "=" )
	> {};

 struct L1_label_rule:
    label_rule {};


 struct gotos:
	TAOCPP_PEGTL_STRING( "goto" ) {};
 
struct line_label:
	pegtl::seq<
		seps,
		pegtl::opt<
			gotos				
		>,
		seps,
		L1_label_rule,
		seps
	> {};


 struct reg_rule:
	pegtl::sor<
		TAOCPP_PEGTL_STRING( "rax" ),
		TAOCPP_PEGTL_STRING( "rdx" ),
		TAOCPP_PEGTL_STRING( "rcx" ),
		TAOCPP_PEGTL_STRING( "rbx" ),	
		TAOCPP_PEGTL_STRING( "rbp" ),
		TAOCPP_PEGTL_STRING( "r8" ),
		TAOCPP_PEGTL_STRING( "r9" ),
		TAOCPP_PEGTL_STRING( "r10" ),
		TAOCPP_PEGTL_STRING( "r11" ),
		TAOCPP_PEGTL_STRING( "r12" ),
		TAOCPP_PEGTL_STRING( "r13" ),
		TAOCPP_PEGTL_STRING( "r14" ),
		TAOCPP_PEGTL_STRING( "r15" ),
		TAOCPP_PEGTL_STRING( "rdi" ),
		TAOCPP_PEGTL_STRING( "rsi" ),
		TAOCPP_PEGTL_STRING( "rsp" )
	> {}; 
  
  struct function_name:
    label_rule {};

  struct number:
    pegtl::seq<
      pegtl::opt<
        pegtl::sor<
          pegtl::one< '-' >,
          pegtl::one< '+' >
        >
      >,
      pegtl::plus< 
        pegtl::digit
      >
    >{};

  struct argument_number:
    number {};

  struct local_number:
    number {} ;

  struct value_rule:
	pegtl::seq<
		seps,
		number,
		seps
	>{} ;

  struct mem_rule:
	pegtl::seq<
		seps,
		TAOCPP_PEGTL_STRING( "mem" ),
		seps,
		reg_rule,
		seps,
		value_rule,
		seps
	>{};


  struct dest_rule:
	pegtl::sor<
		reg_rule,
		mem_rule
	>{};
  
  struct src_rule:
	pegtl::seq<
		seps,
		pegtl::sor<
			reg_rule,
			mem_rule,
			value_rule,
			L1_label_rule
		>,
		seps
	>{};

  struct ret:
	pegtl::seq<
		seps,
		TAOCPP_PEGTL_STRING( "return" ),
		seps
	> {}; 

  struct cond_jump:
	pegtl::seq<
		seps,
		TAOCPP_PEGTL_STRING( "cjump" ),
		seps,
		src_rule,
		seps,
		cmp,
		seps,
		src_rule,
		seps,
		L1_label_rule,
		seps,
		L1_label_rule,
		seps		
	> {};	

  struct comparison_rule:
	pegtl::seq<
		seps,
		dest_rule,
		seps,
		TAOCPP_PEGTL_STRING( "<-" ),
		seps,
		src_rule,
		seps,
		cmp,
		seps,
		src_rule,
		seps
	> {};	

  // ex: rax <- rdi
  struct assignment_rule:
      pegtl::seq<
	seps,
	dest_rule,
	seps,
	TAOCPP_PEGTL_STRING("<-"),
	seps,
	src_rule,		
	seps
      >{};


  struct decInc:
	pegtl::seq<
		seps,
		reg_rule,
		seps,
		aop,
		seps,
		aop,
		seps
	>{};
 
 // arithmetic rule expanded to include sop 
  struct arithmetic_rule:
      pegtl::seq<
	seps,
	dest_rule,
	seps,
	aop,
	seps,
	pegtl::one<'='>,
	seps,
	src_rule,		
	seps
      >{};


  struct run_time_fn:
    pegtl::seq<
      pegtl::plus< 
        pegtl::sor<
          pegtl::alpha,
          pegtl::one< '_' >
        >
      >,
      pegtl::star<
        pegtl::sor<
          pegtl::alpha,
          pegtl::one< '_' >,
          pegtl::one< '-' >,
          pegtl::digit
        >
      >
    > {};
  
  struct call:
	pegtl::seq<
		seps,
		TAOCPP_PEGTL_STRING("call"),
		seps,
		pegtl::sor<
			L1_label_rule,
			reg_rule,
			run_time_fn
		>,
		seps,
		value_rule,
		seps		
	> {};

  struct loadAddress_rule:
	pegtl::seq<
		seps,
		reg_rule,
		seps,
		pegtl::one<'@'>,
		seps,
		reg_rule,
		seps,
		reg_rule,
		seps,
		value_rule,
		seps		
	> {};

  struct body:
    pegtl::star<
	// options here to fall into the correct "rule" for each line
	pegtl::sor<
		comparison_rule,
		ret,
		line_label,
		arithmetic_rule,
		assignment_rule,
		loadAddress_rule,
		decInc,
		cond_jump,
		call
	>
    >{};        

 
  struct L1_function_rule:
    pegtl::seq<
      seps,
      pegtl::one< '(' >,
      function_name,
      seps,
      argument_number,
      seps,
      local_number,
      seps,
      body,
      seps,
      pegtl::one< ')' >,
      seps
    > {};

// assignment rules here

  struct L1_functions_rule:
    pegtl::seq<
      seps,
      pegtl::plus< L1_function_rule >
    > {};

  struct entry_point_rule:
    pegtl::seq<
      seps,
      pegtl::one< '(' >,
      seps,
      label_rule,
      seps,
      L1_functions_rule,
      seps,
      pegtl::one< ')' >,
      seps
    > {};

  struct grammar : 
    pegtl::must< 
      entry_point_rule
    > {};

  /* 
   * Actions attached to grammar rules.
   */
  template< typename Rule >
  struct action : pegtl::nothing< Rule > {};

  template<> struct action < label_rule > {
    template< typename Input >
    static void apply( const Input & in, L1::Program & p){
      if (p.entryPointLabel.empty()){
        p.entryPointLabel = in.string();
      }
    }
  };


 template<> struct action < run_time_fn > {
    template< typename Input >
    static void apply( const Input & in, L1::Program & p){
      L1::Item i;
      i.str = in.string();
      i.t = L1::Type::call_type;
      parsed_registers.push_back(i);
    }
  };


  template<> struct action < function_name > {
    template< typename Input >
    static void apply( const Input & in, L1::Program & p){
      L1::Function *newF = new L1::Function();
      newF->name = in.string();
      p.functions.push_back(newF);
    }
  };

  template<> struct action < L1_label_rule > {
    template< typename Input >
		static void apply( const Input & in, L1::Program & p){
      L1::Item i;
      i.str = in.string();
      i.t = L1::Type::label;
      parsed_registers.push_back(i);
    }
  };


  template<> struct action < line_label > {
    template< typename Input >
    static void apply( const Input & in, L1::Program & p){
      L1::Ln_Label *newL = new L1::Ln_Label();
      newL->type = L1::LineType::lineLabel;
      L1::Item label = parsed_registers.back();
      parsed_registers.pop_back();
      newL->goToFlag = false;
      if (parsed_registers.back().t == g2){
	parsed_registers.pop_back();
	newL->goToFlag = true;
      }
      newL->lab = label.str;
      p.functions.back()->lines.push_back(newL);
    }
  };

  template<> struct action < gotos > {
    template< typename Input >
    static void apply( const Input & in, L1::Program & p){
      L1::Item i;
      i.str = in.string();
      i.t = L1::Type::g2;
      parsed_registers.push_back(i);
    }
  };

  template<> struct action < argument_number > {
    template< typename Input >
    static void apply( const Input & in, L1::Program & p){
      L1::Function *currentF = p.functions.back();
      currentF->arguments = std::stoll(in.string());
    }
  };

  template<> struct action < local_number > {
    template< typename Input >
    static void apply( const Input & in, L1::Program & p){
      L1::Function *currentF = p.functions.back();
      currentF->locals = std::stoll(in.string());
    }
  };
 
  template<> struct action < ret > {
    template< typename Input >
    static void apply( const Input & in, L1::Program & p){
      L1::Instruction *newL = new L1::Instruction();
      newL->type = L1::LineType::Return;
      p.functions.back()->lines.push_back(newL);
    }
  };


  template<> struct action < call > {
    template< typename Input >
    static void apply( const Input & in, L1::Program & p){
       L1::Call_inst *newA = new L1::Call_inst();
       L1::Item num_args = parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item fn = parsed_registers.back();
       parsed_registers.pop_back();
       newA->num_args = num_args.val;
       newA->fn = fn;
       newA->type = L1::LineType::call_line;	   
       p.functions.back()->lines.push_back(newA);
    }
  };

  template<> struct action < loadAddress_rule > {
    template< typename Input >
    static void apply( const Input & in, L1::Program & p){
       L1::LEA *newLea = new L1::LEA();
       L1::Item offset = parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item src2 = parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item src1 = parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item dst = parsed_registers.back();
       parsed_registers.pop_back();
       newLea->offset = offset.val;
       newLea->src2 = src2;
       newLea->src1 = src1;
       newLea->dst = dst;
       newLea->type = L1::LineType::leaType;	   
       p.functions.back()->lines.push_back(newLea);
    }
  };
  
  template<> struct action < assignment_rule > {
    template< typename Input >
    static void apply( const Input & in, L1::Program & p){
       L1::Assignment *newA = new L1::Assignment();
       L1::Item src = parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item dest = parsed_registers.back();
       parsed_registers.pop_back();
       newA->dst = dest;
       newA->src = src;
       newA->type = L1::LineType::Assign;	   
       p.functions.back()->lines.push_back(newA);
    }
  };


  template<> struct action < cmp > {
    template< typename Input >
		static void apply( const Input & in, L1::Program & p){
      L1::Item i; 
      i.str = in.string();
      i.t = L1::Type::operation;
      parsed_registers.push_back(i);
    }
  };


  template<> struct action < aop > {
    template< typename Input >
		static void apply( const Input & in, L1::Program & p){
      L1::Item i;
      i.str = in.string();
      i.t = L1::Type::operation;
      parsed_registers.push_back(i);
    }
  };

 
  template<> struct action < decInc > {
   template< typename Input >
    static void apply( const Input & in, L1::Program & p){
       L1::DI *newA = new L1::DI();
       L1::Item op = parsed_registers.back();
       parsed_registers.pop_back();
       parsed_registers.pop_back();
       L1::Item dest = parsed_registers.back();
       parsed_registers.pop_back();
       newA->dst = dest;
       newA->op = op.str[0];
       newA->type = L1::LineType::decremIncrem;	   
       p.functions.back()->lines.push_back(newA);
    }
  };


  template<> struct action < cond_jump > {
   template< typename Input >
    static void apply( const Input & in, L1::Program & p){
       L1::cJump *newC = new L1::cJump();
       L1::Item label2 = parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item label1= parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item src2 = parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item cmp = parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item src1 = parsed_registers.back();
       parsed_registers.pop_back();
       newC->label2 = label2;
       newC->label1 = label1;
       newC->src2 = src2;
       newC->src1 = src1;
       if (cmp.str.length() > 1)
		newC->c = L1::cmpType::le;
       else if (cmp.str[0] == '=')
		newC->c = L1::cmpType::e;
       else
		newC->c = L1::cmpType::l;
       newC->type = L1::LineType::conditional_jump;	   
       p.functions.back()->lines.push_back(newC);
    }
  };

  template<> struct action < comparison_rule > {
   template< typename Input >
    static void apply( const Input & in, L1::Program & p){
       L1::Compare *newC = new L1::Compare();
       L1::Item src2 = parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item cmp= parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item src1 = parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item dst = parsed_registers.back();
       parsed_registers.pop_back();
       newC->dst = dst;
       newC->src1 = src1;
       newC->src2 = src2;
       if (cmp.str.length() > 1)
		newC->c = L1::cmpType::le;
       else if (cmp.str[0] == '=')
		newC->c = L1::cmpType::e;
       else
		newC->c = L1::cmpType::l;
       newC->type = L1::LineType::Comparison;	   
       p.functions.back()->lines.push_back(newC);
    }
  };


  template<> struct action < arithmetic_rule > {
   template< typename Input >
    static void apply( const Input & in, L1::Program & p){
       L1::Arithmetic *newA = new L1::Arithmetic();
       L1::Item src = parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item op = parsed_registers.back();
       parsed_registers.pop_back();
       L1::Item dest = parsed_registers.back();
       parsed_registers.pop_back();
       newA->dst = dest;
       newA->src = src;
       newA->op = op.str[0];
       newA->type = L1::LineType::Arith;	   
       p.functions.back()->lines.push_back(newA);
    }
  };


  template<> struct action < reg_rule > {
    template< typename Input >
    static void apply( const Input & in, L1::Program & p){
      L1::Item newI;
      newI.str = in.string();
      newI.t = L1::Type::reg;
      parsed_registers.push_back(newI);
    }
  };
 
  template<> struct action <value_rule> {
    template <typename Input>
    static void apply (const Input & in, L1::Program &p) {
      L1::Item newI;
      newI.val = std::stoll(in.string());
      newI.t = L1::Type::value;
      parsed_registers.push_back(newI);
    }
  };

  template<> struct action < mem_rule > {
    template< typename Input >
    static void apply( const Input & in, L1::Program & p){
      L1::Item newI;
      L1::Item offset = parsed_registers.back();
      parsed_registers.pop_back();
      L1::Item reg = parsed_registers.back();
      parsed_registers.pop_back();
      newI.str = reg.str;
      newI.val = offset.val;
      newI.t = L1::Type::mem;
      parsed_registers.push_back(newI);
    }
  };

  Program parse_file (char *fileName){
    /* 
     * Check the grammar for some possible issues.
     */
    pegtl::analyze< L1::grammar >();

    /*
     * Parse.
     */   
    file_input< > fileInput(fileName);
    L1::Program p;
    parse< L1::grammar, L1::action >(fileInput, p);
    return p;
  }

} // L1
