#pragma once

#include <vector>
#include <classes.cpp>

namespace L1 {

  struct L1_item {
    std::string labelName;
  };

  struct Function{
    std::string name;
    int64_t arguments;
    int64_t locals;
    std::vector<Instruction *> lines;
  };


  struct Program{
    std::string entryPointLabel;
    std::vector<L1::Function *> functions;
  };

} // L5
